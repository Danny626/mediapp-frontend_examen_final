import { Component, OnInit } from '@angular/core';
import {Menu} from '../../../_model/menu';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {MenuService} from '../../../_service/menu.service';
import {ActivatedRoute, Params, Router} from '@angular/router';
import {Rol} from '../../../_model/rol';

@Component({
  selector: 'app-menu-edicion',
  templateUrl: './menu-edicion.component.html',
  styleUrls: ['./menu-edicion.component.css']
})
export class MenuEdicionComponent implements OnInit {

  id: number;
  menu: Menu;
  form: FormGroup;
  edicion: boolean = false;

  roles: Rol[];

  constructor(private menuService: MenuService, private route: ActivatedRoute, private router: Router) {
    this.menu = new Menu();

    this.form = new FormGroup({
      'id': new FormControl(0),
      'icono': new FormControl('', [Validators.required, Validators.minLength(3), Validators.maxLength(70)]),
      'nombre': new FormControl('', [Validators.required, Validators.minLength(3), Validators.maxLength(70)]),
      'url': new FormControl('', [Validators.required, Validators.minLength(3), Validators.maxLength(150)]),
    });
  }

  ngOnInit() {
    this.route.params.subscribe((params: Params) => {
      this.id = params['id'];
      this.edicion = params['id'] != null;
      this.initForm();
    });
  }

  private initForm() {
    if (this.edicion) {
      this.menuService.listarMenuPorId(this.id).subscribe(data => {
        let id = data.idMenu;
        let icono = data.icono;
        let nombre = data.nombre;
        let url = data.url;
        this.roles = data.roles;

        this.form = new FormGroup({
          'id': new FormControl(id),
          'icono': new FormControl(icono),
          'nombre': new FormControl(nombre),
          'url': new FormControl(url)
        });
      });
    }
  }

  operar() {
    this.menu.idMenu = this.form.value['id'];
    this.menu.nombre = this.form.value['nombre'];
    this.menu.icono = this.form.value['icono'];
    this.menu.url = this.form.value['url'];
    this.menu.roles = this.roles; //////

    if (this.edicion) {
      //update
      this.menuService.modificar(this.menu).subscribe(data => {
        this.menuService.listarMenus().subscribe(menus => {
          this.menuService.menuCambio.next(menus);
          this.menuService.mensaje.next('Se modificó');
        });
      });
      console.log("update: " + JSON.stringify(this.menu));
    } else {
      //insert
      this.menuService.registrar(this.menu).subscribe(data => {
        this.menuService.listarMenus().subscribe(menus => {
          this.menuService.menuCambio.next(menus);
          this.menuService.mensaje.next('Se registró');
        });
      });
      console.log("insert: " + JSON.stringify(this.menu));
    }

    this.router.navigate(['menu']);
  }

}

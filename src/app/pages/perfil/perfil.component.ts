import { Component, OnInit } from '@angular/core';
import {JwtHelperService} from '@auth0/angular-jwt';
import {TOKEN_NAME} from '../../_shared/var.constant';
import * as decode from 'jwt-decode';

@Component({
  selector: 'app-perfil',
  templateUrl: './perfil.component.html',
  styleUrls: ['./perfil.component.css']
})
export class PerfilComponent implements OnInit {
  roles: string = '';
  user: string = '';

  constructor() {
  }

  ngOnInit() {
    const helper = new JwtHelperService();

    let token = JSON.parse(sessionStorage.getItem(TOKEN_NAME));

    if (!helper.isTokenExpired(token.access_token)) {
      const decodedToken = decode(token.access_token);  //para usar decode se debe instalar: npm install jwt-decode
      this.roles = decodedToken.authorities;
      this.user = decodedToken.user_name;
      this.roles = this.roles.toString().replace(/,/gi, ' | ');
      //console.log(this.roles + ' - ' + this.user);

    }
  }

}

import { Component, OnInit } from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {Rol} from '../../../_model/rol';
import {RolService} from '../../../_service/rol.service';
import {ActivatedRoute, Params, Router} from '@angular/router';

@Component({
  selector: 'app-rol-edicion',
  templateUrl: './rol-edicion.component.html',
  styleUrls: ['./rol-edicion.component.css']
})
export class RolEdicionComponent implements OnInit {

  id: number;
  rol: Rol;
  form: FormGroup;
  edicion: boolean = false;

  constructor(private rolService: RolService, private route: ActivatedRoute, private router: Router) {
    this.rol = new Rol();

    this.form = new FormGroup({
      'id': new FormControl(0),
      'nombre': new FormControl('', [Validators.required, Validators.minLength(3), Validators.maxLength(70)]),
      'descripcion': new FormControl('', [Validators.required, Validators.minLength(3), Validators.maxLength(150)]),
    });
  }

  ngOnInit() {
    this.route.params.subscribe((params: Params) => {
      this.id = params['id'];
      this.edicion = params['id'] != null;
      this.initForm();
    });
  }

  private initForm() {
    if (this.edicion) {
      this.rolService.listarRolPorId(this.id).subscribe(data => {
        let id = data.idRol;
        let nombre = data.nombre;
        let descripcion = data.descripcion;

        this.form = new FormGroup({
          'id': new FormControl(id),
          'nombre': new FormControl(nombre),
          'descripcion': new FormControl(descripcion)
        });
      });
    }
  }

  operar() {
    this.rol.idRol = this.form.value['id'];
    this.rol.nombre = this.form.value['nombre'];
    this.rol.descripcion = this.form.value['descripcion'];

    if (this.edicion) {
      //update
      this.rolService.modificar(this.rol).subscribe(data => {
        this.rolService.listarRoles().subscribe(roles => {
          this.rolService.rolCambio.next(roles);
          this.rolService.mensaje.next('Se modificó');
        });
      });
    } else {
      //insert
      this.rolService.registrar(this.rol).subscribe(data => {
        this.rolService.listarRoles().subscribe(roles => {
          this.rolService.rolCambio.next(roles);
          this.rolService.mensaje.next('Se registró');
        });
      });
    }

    this.router.navigate(['rol']);
  }
}

import {Component, OnInit, ViewChild} from '@angular/core';
import {MatPaginator, MatSnackBar, MatSort, MatTableDataSource} from '@angular/material';
import {RolService} from '../../_service/rol.service';
import {Rol} from '../../_model/rol';

@Component({
  selector: 'app-rol',
  templateUrl: './rol.component.html',
  styleUrls: ['./rol.component.css']
})
export class RolComponent implements OnInit {

  lista: Rol[] = [];
  displayedColumns = ['idRol', 'nombre', 'descripcion', 'acciones'];
  dataSource: MatTableDataSource<Rol>;
  cantidad: number;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  constructor(private rolService: RolService, private snackBar: MatSnackBar) { }

  ngOnInit() {
    this.rolService.rolCambio.subscribe(data => {
      this.lista = data;
      this.dataSource = new MatTableDataSource(this.lista);
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;

      this.rolService.mensaje.subscribe(data2 => {
        this.snackBar.open(data2, 'Aviso', { duration: 2000 });
      });
    });

    this.rolService.listarRolesPageable(0, 10).subscribe(data => {
      let pacientes = JSON.parse(JSON.stringify(data)).content;
      this.cantidad = JSON.parse(JSON.stringify(data)).totalElements;
      this.dataSource = new MatTableDataSource(pacientes);
      this.dataSource.sort = this.sort;
    });
  }

  applyFilter(filterValue: string) {
    filterValue = filterValue.trim();
    filterValue = filterValue.toLowerCase();
    this.dataSource.filter = filterValue;
  }

  eliminar(idRol: number) {
    this.rolService.eliminar(idRol).subscribe(data => {
        this.rolService.listarRoles().subscribe(data2 => {
          this.lista = data2;
          this.dataSource = new MatTableDataSource(this.lista);
          this.dataSource.paginator = this.paginator;
          this.dataSource.sort = this.sort;
        });
      }
    );
  }

  mostrarMas(e: any) {
    console.log(e);
    this.rolService.listarRolesPageable(e.pageIndex, e.pageSize).subscribe(data => {
      console.log(data);
      let pacientes = JSON.parse(JSON.stringify(data)).content;
      this.cantidad = JSON.parse(JSON.stringify(data)).totalElements;

      this.dataSource = new MatTableDataSource(pacientes);
      this.dataSource.sort = this.sort;
    });
  }

}

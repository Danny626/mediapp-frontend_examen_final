import { Component, OnInit } from '@angular/core';
import {Signo} from '../../../_model/signo';
import {SignoService} from '../../../_service/signo.service';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {ActivatedRoute, Params, Router} from '@angular/router';
import {PacienteService} from '../../../_service/paciente.service';
import {Paciente} from '../../../_model/paciente';
import {MatDialog, MatSnackBar, MatTableDataSource} from '@angular/material';
import {Observable} from 'rxjs';
import {map, startWith} from 'rxjs/operators';
import {PacienteEdicionComponent} from '../../paciente/paciente-edicion/paciente-edicion.component';
import {DialogoPacienteComponent} from './dialogo-paciente/dialogo-paciente.component';
import {log} from 'util';

@Component({
  selector: 'app-signo-edicion',
  templateUrl: './signo-edicion.component.html',
  styleUrls: ['./signo-edicion.component.css']
})
export class SignoEdicionComponent implements OnInit {

  id: number;
  signo: Signo;
  form: FormGroup;  //el nombre "form" debe coincidir con nombre del formulario: [formGroup]="form" (en el *html)
  edicion: boolean = false;

  //fechaSeleccionada: Date = new Date();
  maxFecha: Date = new Date();
  fechaSigno = new FormControl();

  pacientes: Paciente[] = [];
  strPacienteRecovered: string;
  filteredOptions: Observable<any[]>;
  myControlPaciente: FormControl = new FormControl();
  pacienteSeleccionado: Paciente;

  constructor(private signoService: SignoService, private route: ActivatedRoute,
              private pacienteService: PacienteService,
              private router: Router,
              public snackBar: MatSnackBar,
              public signoDialog: MatDialog) {
    this.signo = new Signo();

    this.form = new FormGroup({
      'id': new FormControl(0),
      'paciente': this.myControlPaciente,
      //'paciente': new FormControl('', [Validators.required, Validators.minLength(3), Validators.maxLength(70)]),
      //'fecha': new FormControl('', [Validators.required, Validators.minLength(3), Validators.maxLength(70)]),
      'temperatura': new FormControl('', [Validators.required, Validators.minLength(8), Validators.maxLength(8)]),
      'pulso': new FormControl('', [Validators.required, Validators.minLength(8), Validators.maxLength(8)]),
      'ritmo': new FormControl('', [Validators.required, Validators.minLength(8), Validators.maxLength(8)])
    });
  }

  ngOnInit() {
    //Se cargar al cargar la página
    this.listarPacientes();

    //Se carga cuando detecta cambios en "pacienteCambio"
    this.pacienteService.pacienteCambio.subscribe(pacientes => {
      console.log("Recarga....................");
      this.pacientes = pacientes;
      this.pacienteService.mensaje.subscribe(msj => {
        this.snackBar.open(msj, 'Aviso', { duration: 2000 });
      });
    });

    this.filteredOptions = this.myControlPaciente.valueChanges.pipe(
      startWith(null),
      map(val => this.filter(val))
    );

    this.route.params.subscribe((params: Params) => {
      this.id = params['id'];
      this.edicion = params['id'] != null;
      this.initForm();
    });

  }

  filter(val: any) {
    if (val != null && val.idPaciente > 0) {
      return this.pacientes.filter(option =>
        option.nombres.toLowerCase().includes(val.nombres.toLowerCase()) || option.apellidos.toLowerCase().includes(val.apellidos.toLowerCase()) || option.dni.includes(val.dni));
    } else {
      return this.pacientes.filter(option =>
        option.nombres.toLowerCase().includes(val.toLowerCase()) || option.apellidos.toLowerCase().includes(val.toLowerCase()) || option.dni.includes(val));
    }
  }

  displayFn(val: Paciente) {
    //console.log("displayFn: " + val.nombres);
    return val ? `${val.nombres} ${val.apellidos}` : val;
  }

  seleccionarPaciente(e) {
    console.log("seleccionarPaciente: " + e.option.value.apellidos);
    this.pacienteSeleccionado = e.option.value;
  }

  private initForm() {
    if (this.edicion) {
      //cargar data inicial
      this.signoService.listarSignoPorId(this.id).subscribe(data => {
        let id = data.idSigno;
        let paciente = data.paciente;
        let fecha = data.fecha;
        let temperatura = data.temperatura;
        let pulso = data.pulso;
        let ritmoRespiratorio = data.ritmoRespiratorio;

        this.strPacienteRecovered = `${paciente.nombres} ${paciente.apellidos}`;
        this.pacienteSeleccionado = paciente;
        //this.displayFn(paciente);

        //this.myControlPaciente = new FormControl(paciente);
        this.form = new FormGroup({
          'id': new FormControl(id),
          //'paciente': new FormControl(paciente),
          'paciente': this.myControlPaciente,
          //'fecha': new FormControl(fecha),
          'temperatura': new FormControl(temperatura),
          'pulso': new FormControl(pulso),
          'ritmo': new FormControl(ritmoRespiratorio)
        });
        this.fechaSigno = new FormControl(new Date(fecha));

        //this.pacientes.filter();
        //this.seleccionarPaciente(paciente);
        //this.filter(`${paciente.nombres} ${paciente.nombres}`);
        //this.displayFn(paciente);

        //this.fechaSeleccionada = new Date(fecha);
        //fechaSeleccionada2 = new FormControl((new Date()).toISOString());
      });
    }
  }

  operar() {
    this.signo.idSigno = this.form.value['id'];
    //this.signo.paciente = this.form.value['paciente']; // pacienteSeleccionado
    this.signo.paciente = this.pacienteSeleccionado;
    //this.signo.fecha = this.form.value['fecha'];
    this.signo.fecha = new Date(this.fechaSigno.value).toISOString();
    //console.log(new Date(this.signo.fecha).toLocaleDateString());
    this.signo.temperatura = this.form.value['temperatura'];
    this.signo.pulso = this.form.value['pulso'];
    this.signo.ritmoRespiratorio = this.form.value['ritmo'];
    /*
    let tzoffset = (this.fechaSigno.value).getTimezoneOffset() * 60000; //offset in milliseconds
    let localISOTime = (new Date(Date.now() - tzoffset)).toISOString();
    this.signo.fecha = localISOTime;
    */

    if (this.edicion) {
      //update
      this.signoService.modificar(this.signo).subscribe(data => {
        this.signoService.listarSignos().subscribe(signos => {
          this.signoService.signoCambio.next(signos);
          this.signoService.mensaje.next('Se modificó');
          this.limpiarControles();
        });
      });
    } else {
      //insert
      this.signoService.registrar(this.signo).subscribe(data => {
        this.signoService.listarSignos().subscribe(signos => {
          this.signoService.signoCambio.next(signos);
          this.signoService.mensaje.next('Se registró');
          this.limpiarControles();
        });
      });
    }
    this.router.navigate(['signo']);
  }//Operar

  limpiarControles() {
    this.pacienteSeleccionado = null;
    this.fechaSigno = new FormControl();
    /*
    this.fechaSeleccionada = new Date();
    this.fechaSeleccionada.setHours(0);
    this.fechaSeleccionada.setMinutes(0);
    this.fechaSeleccionada.setSeconds(0);
    this.fechaSeleccionada.setMilliseconds(0);
    */
  }

  listarPacientes() {
    this.pacienteService.listarPacientes().subscribe(data => {
      this.pacientes = data;
    });
  }

  openDialog(): void {

    //this.router.navigate(['signo', 'nuevo']);

    //document.getElementById('botonDialog').click();
    //this.router.navigate(['signo', 'nuevo' ], { relativeTo: this.route});
    //this.router.navigate(['signo', 'nuevo'], { replaceUrl: true });
    const dialogRef = this.signoDialog.open(DialogoPacienteComponent, {
      width: '550px',
      disableClose: false,
      data: {idSigno_: this.form.value['id']}
    });

    dialogRef.afterClosed().subscribe( (result: Paciente) => {
      console.log('The dialog was closed');
      //VALOR RETORNADO con SUSCRIBE
      if (result != null) {
        console.log("RESULTADO: " + result.idPaciente + " - " + result.nombres);
        this.strPacienteRecovered = `${result.nombres} ${result.apellidos}`;
        this.pacienteSeleccionado = result;
      } else {
        console.log("VACIO");
      }
    });
  }

}

import {Component, OnInit, ViewChild} from '@angular/core';
import {MatDialog, MatPaginator, MatSnackBar, MatSort, MatTableDataSource} from '@angular/material';
import {Signo} from '../../_model/signo';
import {SignoService} from '../../_service/signo.service';

@Component({
  selector: 'app-signo',
  templateUrl: './signo.component.html',
  styleUrls: ['./signo.component.css']
})
export class SignoComponent implements OnInit {

  lista: Signo[] = [];
  displayedColumns = ['idSigno', 'paciente', 'fecha', 'signos', 'acciones'];
  dataSource: MatTableDataSource<Signo>;
  cantidad: number;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  constructor(private signoService: SignoService, private snackBar: MatSnackBar) { }

  ngOnInit() {
    this.signoService.signoCambio.subscribe(data => {
      this.lista = data;
      this.dataSource = new MatTableDataSource(this.lista);
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;

      this.signoService.mensaje.subscribe(data2 => {
        this.snackBar.open(data2, 'Aviso', { duration: 2000 });
      });
    });

    this.signoService.listarSignosPageable(0, 10).subscribe(data => {
      let signos = JSON.parse(JSON.stringify(data)).content;
      this.cantidad = JSON.parse(JSON.stringify(data)).totalElements;
      this.dataSource = new MatTableDataSource(signos);
      this.dataSource.sort = this.sort;
    });
  }//OnInit

  applyFilter(filterValue: string) {
    filterValue = filterValue.trim();
    filterValue = filterValue.toLowerCase();
    this.dataSource.filter = filterValue;
  }

  eliminar(idPaciente: number) {
    this.signoService.eliminar(idPaciente).subscribe(data => {
        this.signoService.listarSignos().subscribe(data2 => {
          this.lista = data2;
          this.dataSource = new MatTableDataSource(this.lista);
          this.dataSource.paginator = this.paginator;
          this.dataSource.sort = this.sort;
        });
      }
    );
  }

  mostrarMas(e: any) {
    console.log(e);
    this.signoService.listarSignosPageable(e.pageIndex, e.pageSize).subscribe(data => {
      console.log(data);
      let signos = JSON.parse(JSON.stringify(data)).content;
      this.cantidad = JSON.parse(JSON.stringify(data)).totalElements;

      this.dataSource = new MatTableDataSource(signos);
      this.dataSource.sort = this.sort;
    });
  }

}

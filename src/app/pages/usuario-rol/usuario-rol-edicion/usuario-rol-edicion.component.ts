import { Component, OnInit } from '@angular/core';
import {Usuario} from '../../../_model/usuario';
import {UsuarioService} from '../../../_service/usuario.service';
import {ActivatedRoute, Params, Router} from '@angular/router';
import {Rol} from '../../../_model/rol';
import {MatSnackBar} from '@angular/material';
import {RolService} from '../../../_service/rol.service';

@Component({
  selector: 'app-usuario-rol-edicion',
  templateUrl: './usuario-rol-edicion.component.html',
  styleUrls: ['./usuario-rol-edicion.component.css']
})
export class UsuarioRolEdicionComponent implements OnInit {

  id: number;
  usuario: Usuario;
  edicion: boolean = false;

  idUsuarioBypass: number;
  usernameBypass: string;
  passwordBypass: string;
  enabledBypass: boolean;
  rolesBypass: Rol[];
  roles: Rol[];

  idRolSeleccionado: number;
  rolesSeleccionados: Rol[] = [];
  mensaje: string;

  constructor(private usuarioService: UsuarioService, private rolService: RolService, private route: ActivatedRoute, private router: Router,
              public snackBar: MatSnackBar) {
    this.usuario = new Usuario();
  }

  ngOnInit() {
    this.route.params.subscribe((params: Params) => {
      this.id = params['id'];
      this.edicion = params['id'] != null;
      this.initForm();
      this.limpiarControles();
    });

    this.listarRoles();
  }

  private initForm() {
    if (this.edicion) {
      this.usuarioService.listarUsuarioPorId(this.id).subscribe(data => {
        this.idUsuarioBypass = data.idUsuario;
        this.usernameBypass = data.username;
        this.passwordBypass = data.password;
        this.enabledBypass = data.enabled;
        this.rolesBypass = data.roles;
      });
    }
  }

  operar() {
    let usuario = new Usuario();
    usuario.idUsuario = this.idUsuarioBypass;
    usuario.username = this.usernameBypass;
    usuario.password = this.passwordBypass;
    usuario.enabled = this.enabledBypass;

    let rolesTotales = this.rolesBypass.concat(this.rolesSeleccionados);
    usuario.roles = rolesTotales;
    //usuario.roles = this.rolesSeleccionados;

    this.usuarioService.modificar(usuario).subscribe(data => {
      this.usuarioService.listarUsuarios().subscribe(roles => {
        this.usuarioService.usuarioCambio.next(roles);
        this.usuarioService.mensaje.next('Se actualizó');
        this.router.navigate(['usuario-rol']);
      });
    });

  }// Operar

  listarRoles() {
    this.rolService.listarRoles().subscribe(data => {
      this.roles = data;
    });
  }

  agregarRol() {
    if (this.idRolSeleccionado > 0) {
      let cont = 0; let cont2 = 0;
      //Evitar roles duplicados: Comparar con lista de ROLES nuevos recién asignados
      for (let i = 0; i < this.rolesSeleccionados.length; i++) {
        let rol = this.rolesSeleccionados[i];
        if (rol.idRol === this.idRolSeleccionado) {
          cont++;
          break;
        }
      }
      //Evitar roles duplicados: Comparar con lista de ROLES ya existentes
      for (let i = 0; i < this.rolesBypass.length; i++) {
        let rol = this.rolesBypass[i];
        if (rol.idRol === this.idRolSeleccionado) {
          cont2++;
          break;
        }
      }
      if (cont > 0) {
        this.mensaje = `Rol duplicado: El rol se acaba de asignar.`;
        this.snackBar.open(this.mensaje, "Aviso", { duration: 3000 });
      } else
      if (cont2 > 0) {
        this.mensaje = `Rol duplicado: El rol ya se encuentra asignado.`;
        this.snackBar.open(this.mensaje, "Aviso", { duration: 3000 });
      } else {
        let rol_ = new Rol();
        this.rolService.listarRolPorId(this.idRolSeleccionado).subscribe(data => {
          //Chasky:
          rol_ = data;
          this.rolesSeleccionados.push(rol_);
        // rol_.idRol = this.idRolSeleccionado;
        // this.rolService.listarRolPorId(this.idRolSeleccionado).subscribe(data => {
        //   rol_.nombre = data.nombre;
        //   this.rolesSeleccionados.push(rol_);
        });
      }
    } else {
      this.mensaje = `Debe agregar un rol`;
      this.snackBar.open(this.mensaje, "Aviso", { duration: 2000 });
    }
  }

  removerRol(index: number) {
    this.rolesSeleccionados.splice(index, 1);
  }

  removerRolBypass(index: number) {
    this.rolesBypass.splice(index, 1);
  }

  limpiarControles() {
    this.rolesSeleccionados = [];
    this.idRolSeleccionado = 0;
    this.rolesBypass = [];
  }


}

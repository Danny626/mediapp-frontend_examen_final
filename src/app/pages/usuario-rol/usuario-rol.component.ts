import {Component, OnInit, ViewChild} from '@angular/core';
import {Usuario} from '../../_model/usuario';
import {MatPaginator, MatSnackBar, MatSort, MatTableDataSource} from '@angular/material';
import {UsuarioService} from '../../_service/usuario.service';

@Component({
  selector: 'app-usuario-rol',
  templateUrl: './usuario-rol.component.html',
  styleUrls: ['./usuario-rol.component.css']
})
export class UsuarioRolComponent implements OnInit {

  lista: Usuario[] = [];
  displayedColumns = ['idUsuario', 'username', 'enabled', 'acciones'];
  dataSource: MatTableDataSource<Usuario>;
  cantidad: number;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;

  constructor(private usuarioService: UsuarioService, private snackBar: MatSnackBar) { }

  ngOnInit() {
    this.usuarioService.usuarioCambio.subscribe(data => {
      this.lista = data;
      this.dataSource = new MatTableDataSource(this.lista);
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;

      this.usuarioService.mensaje.subscribe(data2 => {
        this.snackBar.open(data2, 'Aviso', { duration: 2000 });
      });
    });

    this.usuarioService.listarUsuariosPageable(0, 10).subscribe(data => {
      let menus = JSON.parse(JSON.stringify(data)).content;
      this.cantidad = JSON.parse(JSON.stringify(data)).totalElements;
      this.dataSource = new MatTableDataSource(menus);
      this.dataSource.sort = this.sort;
    });
  }

  applyFilter(filterValue: string) {
    filterValue = filterValue.trim();
    filterValue = filterValue.toLowerCase();
    this.dataSource.filter = filterValue;
  }

  eliminar(idRol: number) {
    this.usuarioService.eliminar(idRol).subscribe(data => {
        this.usuarioService.listarUsuarios().subscribe(data2 => {
          this.lista = data2;
          this.dataSource = new MatTableDataSource(this.lista);
          this.dataSource.paginator = this.paginator;
          this.dataSource.sort = this.sort;
        });
      }
    );
  }

  mostrarMas(e: any) {
    console.log(e);
    this.usuarioService.listarUsuariosPageable(e.pageIndex, e.pageSize).subscribe(data => {
      console.log(data);
      let menus = JSON.parse(JSON.stringify(data)).content;
      this.cantidad = JSON.parse(JSON.stringify(data)).totalElements;

      this.dataSource = new MatTableDataSource(menus);
      this.dataSource.sort = this.sort;
    });
  }
}
